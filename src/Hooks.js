import React, { useState, useEffect } from "react";

const Hooks = () => {
    const [count, setCount] = useState(0);
    useEffect(() => {
        count++
        setCount(count)
    })

    return (
        <>
            <h1>Count: {count}</h1>
        </>
    )
}

export default Hooks;
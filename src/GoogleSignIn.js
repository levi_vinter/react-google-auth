import React, { useEffect } from "react";

window.handleGoogleAuth = (response) => {
  console.log(response.credential)
  fetch(
    "http://localhost:5293/api/auth/login",
    {
      method: "POST",
      body: JSON.stringify({ token: response.credential }),
      headers: { "Content-Type": "application/json" }
    })
};


const GoogleSignIn = () => {
  useEffect(() => {
    // <script src="https://accounts.google.com/gsi/client" async defer></script>
    const script = document.createElement("script");
    script.src = "https://accounts.google.com/gsi/client";
    script.async = true;
    script.defer = true;
    document.body.appendChild(script);

    return () => {
      document.body.removeChild(script);
    };
  }, []);

  return (
    <>
      <div
        id="g_id_onload"
        data-client_id="557079019771-36e0cpdck27msefa3g6h4fmres7l1qig.apps.googleusercontent.com"
        data-context="signin"
        data-ux_mode="popup"
        data-callback="handleGoogleAuth"
        data-auto_prompt="false"
      />
      <div
        className="g_id_signin"
        data-type="standard"
        data-shape="rectangular"
        data-theme="outline"
        data-text="signin_with"
        data-size="large"
        data-logo_alignment="left"
      />
    </>
  );
};

export default GoogleSignIn;

import React, { useEffect, useState } from 'react';

const handleCredentialResponse = (response) => {
    console.log(response.credential);
}

const GoogleSignIn = () => {
  const [gapiLoaded, setGapiLoaded] = useState(false);

  useEffect(() => {
    const script = document.createElement('script');
    script.src = 'https://accounts.google.com/gsi/client';
    script.async = true;
    script.defer = true;
    script.onload = () => {
      setGapiLoaded(true);
      window.gapi.load('auth2', () => {
        window.gapi.auth2.init({
          client_id: '557079019771-36e0cpdck27msefa3g6h4fmres7l1qig.apps.googleusercontent.com',
          cookiepolicy: 'single_host_origin',
          scope: 'profile email',
          callback: handleCredentialResponse,
          prompt: 'none'
        });
      });
    };
    document.body.appendChild(script);

    return () => {
      document.body.removeChild(script);
    };
  }, []);

  const handleSignIn = () => {
    window.gapi.auth2.getAuthInstance().signIn();
  };

  return (
    <div>
      {gapiLoaded && (
        <button
          className='g_id_signin'
          onClick={handleSignIn}
        >
          Sign in with Google
        </button>
      )}
    </div>
  );
};

export default GoogleSignIn;